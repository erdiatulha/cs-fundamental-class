let n = 5;
let string = "";
// Upside pyramid
for (let i = 1; i <= n; i++) {
    // prin spasinya
    for (let j = n; j > i; j--) {
        string += " ";
    }
    // prin bintangnya
    for (let k = 0; k < i * 2 - 1; k++) {
        string += "*";
    }
    //newlinenya
    string += "\n";
}
// downside pyramid
for (let i = 1; i <= n - 1; i++) {
    // prin spasinya
    for (let j = 0; j < i; j++) {
        string += " ";
    }
    // prin bintangnya
    for (let k = (n - i) * 2 - 1; k > 0; k--) {
        string += "*";
    }
    //newlinenya
    string += "\n";
}
console.log(string);