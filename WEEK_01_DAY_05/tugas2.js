const { regencies } = require('./location');

function findCityMoreThanWords(n) {
    let result = [];

    for (let regency of regencies) {
        if (regency.name.split(" ").length == n) {
            result.push(regency.name);
        }
    }
    return result;
}
console.log(findCityMoreThanWords(5));
console.log(findCityMoreThanWords(4));
