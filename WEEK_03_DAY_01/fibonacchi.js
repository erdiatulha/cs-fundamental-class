//fibonacci adalah sebuah deret angka dmn angka yg dihasilkan adalah penjumlahan angka sebelumnya
//deret fibonacci : 0, 1, 1, 2, 3, 5, 8, 13 dst
//secara sederhana , untuk mendptkan fibonacci ke-n / fibonacci ke-6 adalah dg menghitung menggunakan 
//fibonacci 6 = fibonacci 5 + fibonacci 4
//=5+3=8

// selain itu, kita juga dpt menggunakan proses rekursif untuk mendptkan fibonacci
//step 1 dimulai dg mendefinisikan fungsi/functionnya ke- n
//step 2 , kita tntukan base case atau titik berhenti dari proses rekursifnya
//next step, kita do the work yaitu membuat sbuah fungsi /recursion call nya agar berhenti dan tdk terjadi looping trs 


function fibonacci(n) { //function ini digunakan u/ menghitung nilai ke-n pd deret fibonacci melalui rekursif 
    if (n == 0) {       // logic = Nilai fibonacci sebuah bilangan adalah jumlah dari nilai fibonacci dua bilangan sebelumnya, 
        return 0;       //kecuali untuk dua bilangan pertama 0 dan 1 yang nilai fibonaccinya juga adalah 0 dan 1 sehingga,
    } else if (n == 1) {  //dibuatkan kondisi bahwa jika n==0 / fibonacci 0 maka mengembalikan/menghasilkan nilai 0
        return 1;          //sdgkan jika n==1 /fibonacci 1 maka akan menghasilkan nilai 1
    } else {               //namun, jika kedua kondisi tdk terpenuhi dimn n>1 
        return fibonacci(n - 1) + fibonacci(n - 2); //maka akan memanggil fungsinya sndri/disebut recursion call
    }           //misalnya n=6 maka fibonacci(6-1)+fibonacci(6-2)=fibonacci(5)+fibonacci(4)
    //next didptkan n=5 dan n=4 lalu dicek kembali apakah =0 dan 1 jika tdk memenuhi maka akan kembali ke recursion call dst
    //sehingga didapatkan hasil akhir bahwa fibonacci ke-6 /fibonacci(6) = 8
}
console.log(fibonacci(6));