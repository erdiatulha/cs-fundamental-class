//Fungsi  rekursif  adalah  fungsi  yang  memanggil  dirinya  sendiri.    
//Fungsi  ini  akan  terus berjalan  sampai  kondisi  berhenti  terpenuhi,  
//oleh  karena  itu  dalam  sebuah  fungsi  rekursif  perlu terdapat  2  blok  penting,  
//yaitu  blok  yang  menjadi  titik  berhenti  dari  sebuah  proses  rekursif  dan blok yang memanggil dirinya sendiri(fungsi/recursion call).


//FPB (50,20)
//a=50 =2x5x5 
//b=20 =2x2x5
// FPB adalah hasil kali dari faktor yg sama dari dua bil dengan mengambil faktor dg pangkat trkecil
//biasanya , secara sederhana untuk mencari FPB dari dua bil kita menggunakan
//faktorisasi prima /pohon faktor sehingga menghasilkan,
//FPB dari 50 dan 20 adalah 2x5 =10
//melalui rekursif dibuat 3 kondisi dimana titik berhenti jika sisa bagi 0 == bil a dengan mengembalikan bil b sbg hasil rekursif.
//logic : 50 dan 20 , 50/20 = 2 sisa bagi 10 , 20/sisa bagi = 20/10 = 2 dg sisa bagi 0 
//maka hasil FPB = nilai bil terakhir pembagian / nilai bil b yg menghasilkan sisa bagi 0 sehingga jwbnnya adalah 10.


function fpb(a, b) { //function ini digunakan u/ mencari nilai FPB melalui rekursif dengan membandingkan 2 bilangan yaitu a dan b / 50 dan 20
    //Basecase
    if (a == 0) {   // jika (50==0) tidak memenuhi , maka
        return b; //titik berhentinya disini 
    } else if (a < b) {  // apakah (50<20) ? 
        return fpb(b, a); // jika iya , maka fbp dari dua bil dbalik sehingga menjadi ..
    } else { // jika tidak, maka                    
        //recursion call 
        return fpb(a % b, b); // fpb akan dimasukkan ke dalam fungsi yaitu (50%20, 20) 
    }                                  // =nilai 2 dg sisa bagi 10 dimana 10 akan menjadi nilai bil a
}                                      //kemudian sisa bagi 10 tadi dicek kembali apakah == 0, jika tdk maka dicek kembali
//apakah (10<20), jika iya maka fbp dari dua bil tsb dbalik sehingga menjadi (20,10)
//kemudian dicek lagi apakah (20==0) jika tidak memnuhi maka apakah (20<10)? jika tdk maka,
//akan dimasukkan kedlm fungsi (20%10, 10) menghasilkan nilai 2 dg sisa bagi 0 
//menjadi (0,10) dimana 0 menjadi bil a , kemudian dicek kembali nilai a , apakah sudah == 0 ,jika iya 
//maka akan mengembalikan nilai b yaitu bil 10 sehingga ketika diconsole / diprint menghasilkan nilai 10.
console.log(fpb(50, 20));

//menurut stackoverflow.com, KPK adalah hasil kali dari bil a dan bil b kemudian dibagi dg FPB dari dua bil.
// so,
function kpk(a, b) {
    return a * b / fpb(a, b);
}
console.log(kpk(50, 20));

