function convertRomanToNumber(roman) {
    const romanMapping = ["I", "V", "X", "L", "C", "D", "M"]
    const valueMapping = [1, 5, 10, 50, 100, 500, 1000]

    let result = 0;

    for (i = 0; i < roman.length; i++) {
        const currentRomanIndex = romanMapping.indexOf(roman[i]);
        const nextRomanIndex = romanMapping.indexOf(roman[i + 1]);

        const currentIndexValue = valueMapping[currentRomanIndex];
        const nextIndexValue = valueMapping[nextRomanIndex];

        if (currentIndexValue < nextIndexValue) {
            result -= currentIndexValue;
        } else {
            result += currentIndexValue;
        }
    }
    return result;
}
console.log(convertRomanToNumber("MMMDCCCXLIX"));